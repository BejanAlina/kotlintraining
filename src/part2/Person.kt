package curs1.part2

open class Person(
    val name: String,
    val state: State = State.HAPPY
) : Sing {
    // # dupa visibility modifiers
    /**
     * Accessing a backing field from a getter/setter
     */
    var address: String = "unspecified"
        set(value) {
            println("Setting the value $value for address")
            field = value
        }

    /**
     * Representing and handling choices enums and "when"
     */
    fun expressFeeling() {
        when (state) {
            State.SAD -> println("I am sad today :( ")
            State.HAPPY -> println("I am happy today :) ")
        }
    }
}

interface Sing {
    fun sing() = println("LALALALLALALALALALALA")
}

/**
 * Representing and handling choices: enums and "when"
 */
enum class State {
    SAD,
    HAPPY
}