package curs1.part2

data class Employee(
    val employeeName: String,
    val salary: Int
) : Person(employeeName)