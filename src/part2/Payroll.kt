package curs1.part2

object Payroll {
    val allEmployees = arrayListOf<Employee>()
    var total = 0

    fun calculateSalary() {
        for (employee in allEmployees) {
            total += employee.salary
        }
        println(total)
    }
}