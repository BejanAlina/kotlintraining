package curs1.part2


fun main() {
    val person = Person("Maria")
    println(person)
    person.sing()

    val emp4 = Employee("e4", 1000)
    println(emp4.sing())

    //visibility modifiers | accessing a backing field from a setter/getter
    person.address = "Bucharest"

    // Representing and handling choices enums and "when"
    person.expressFeeling()

    Payroll.allEmployees.add(Employee("e1", 1000))
    Payroll.allEmployees.add(Employee("e2", 2000))
    println(Payroll.calculateSalary())
    Payroll.allEmployees.add(Employee("e3", 500))
    println(Payroll.calculateSalary())
}