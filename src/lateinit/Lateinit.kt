package curs1.lateinit

import kotlin.test.assertEquals

class MyService {
    fun performAction(): String = "foca"
}

class MyTest {
    private var myService: MyService? = null

    fun setUp() {
        myService = MyService()
    }

    fun testAction() {
        assertEquals("foo", myService!!.performAction())
    }

}

fun main() {
    val myTest = MyTest()
    myTest.setUp()
    myTest.testAction()
}