package curs1.introduction

/**
 * Class documentation
 */
fun main() {
    // comments
    println("HelloWorld") // single line comment
    // single line comment
    /*
    print()
     */

    println("Hello world")
    // data types
    val a: Int = 5
    val string: String = "hello"
    val doublenr: Double = 1.4

    // mutable/ immutable types
    var number = 5
    number = 10
    val immutableNumber = 5
    //immutableNumber = 10
}

fun easierStringFormatting() {
    val number = 5
    println("number" + number)
    println("number $number")
}

/**
 * Declaring functions
 */
fun sum(a: Int, b: Int) = a + b

fun sum2(a: Int, b: Int): Int {
    return a + b
}

/**
 * Default parameters
 */
fun sayHello(first: String = "Mr", last: String) {
    println("Hello $first $last")
}

/**
 * NullableTypes
 */
fun nullableTypes() {
    var nullableVar: String? = "Hello World"

    nullableVar = null
    //number = null

    // Working with nullable types :

    //Safe call operator
    val nullableVarLength = if (nullableVar != null) nullableVar.length else null
    val nullableVarLength2 = nullableVar?.length

    println("nullableVar vs nullableVarLength2 $nullableVarLength $nullableVarLength2")

    //Elvis operator ; using elvis operator to deal with null values;
    val nullableVarSafeLength = if (nullableVar != null) nullableVar.length else 16
    val nullableVarSafeLength2 = nullableVar?.length ?: 16

    println("nullableVarSafeLength vs nullableVarSafeLength2 $nullableVarLength $nullableVarLength2")
}




